//---------------------------------------------------------------------------

#pragma hdrstop

#include "CPPComputator.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

__fastcall TCPPComputator::TCPPComputator() : TComputator(), name("cpp computator") {

};

__fastcall TCPPComputator::~TCPPComputator(){

};

double __fastcall TCPPComputator::Compute(const double Left, const double Right){
    return Left * 0.3 + Right * 0.3;
}

extern "C" __declspec(dllexport) TComputator* __stdcall CreateCppDescendant() {
    return new TCPPComputator();
}
