//---------------------------------------------------------------------------

#ifndef CPPComputatorH
#define CPPComputatorH
//---------------------------------------------------------------------------
#endif
#include "BackendPluginInterface.hpp"
#include <vector>

class __declspec(delphiclass) TCPPComputator : public TComputator {
private:
    String name;
public:
	__fastcall TCPPComputator();
	__fastcall virtual ~TCPPComputator();

	virtual double __fastcall Compute(const double Left, const double Right);
};
